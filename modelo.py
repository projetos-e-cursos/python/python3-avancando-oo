class Programa:

    def __init__(self, nome, ano):
        self._nome = nome
        self.ano = ano
        self._likes = 0

    def dar_like(self):
        self._likes += 1

    @property
    def likes(self):
        return self._likes

    @property
    def nome(self):
        return str(self._nome).title()

    @nome.setter
    def nome(self, novo_nome):
        self._nome = novo_nome

    def __str__(self):
        return f'{self._nome} - {self.ano} - {self._likes}'


class Filme(Programa):

    def __init__(self, nome, ano, duracao):
        super(Filme, self).__init__(nome, ano)
        self.duracao = duracao

    def __str__(self):
        return f'{self._nome} - {self.ano} - {self.duracao} min - {self._likes} likes'


class Serie(Programa):

    def __init__(self, nome, ano, temporadas):
        super(Serie, self).__init__(nome, ano)
        self.temporadas = temporadas

    def __str__(self):
        return f'{self._nome} - {self.ano} - {self.temporadas} temp - {self._likes} likes'


class Playlist:
    def __init__(self, nome, programas):
        self.nome = nome
        self._programas = programas

    def __getitem__(self, item):
        return self._programas[item]

    def __len__(self):
        return len(self._programas)

    @property
    def listagem(self):
        return self._programas

vingadores = Filme('Vingadores Guerra Infinita', 2018, 160)
atlanta = Serie('atlanta', 2018, 2)
tmep = Filme('Todo mundo em pânico', 1999, 100)
demolidor = Serie('Demolidor', 2016, 3)

atlanta.dar_like()
atlanta.dar_like()
vingadores.dar_like()
vingadores.dar_like()
vingadores.dar_like()
tmep.dar_like()
demolidor.dar_like()
demolidor.dar_like()
demolidor.dar_like()

filmes_e_series = [vingadores, atlanta, tmep, demolidor]
fim_de_semana = Playlist('Fim de Semana', filmes_e_series)

print(f'Tamanho do playlist: {fim_de_semana.tamanho}')
for programa in fim_de_semana:
    print(programa)

print(f'Tá ou não tá? {demolidor in fim_de_semana}')
