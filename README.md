# Python3 avançando na Orientação a Objetos

### Conceitos abordados:
- Revisão classes, atributos e métodos;
- Revisão métodos de acessos e atributos privados;
- Name Mangling;
- __Herança__;
- Abstração;
- Variável "protegida" (convenção apenas);
- Método ___super()___
- Relacionamento "__é um__";
- Representação de um objeto;
- Encapsulamento;
- Herança de um tipo _built-in_;
- Python Data Model;
- ABC (Abstract Base Classes);
- Duck Typing;
- Dunder methods;
- Herança múltipla;
- MRO;
- Mixin.

![pythonOO](/uploads/b14b7df615f3b322efc874607cec2497/pythonOO.gif)
<br>Fonte: dicasdepython.com.br